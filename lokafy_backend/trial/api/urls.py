from django.urls import path
from .views import hello_world, get_reviews
urlpatterns = [
    path('hello/', hello_world, name='hello_world'),
    path('reviews/<int:page>/', get_reviews, name='get_reviews')
]
