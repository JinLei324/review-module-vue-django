import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from .models import Review
from .models import City
from .serializers import ReviewSerializer
import csv
import os


class TestRest(TestCase):
    def setUp(self):
        self.client = Client()

        # Read Test datas from csv
        tests_data_dir = os.path.join(os.path.dirname(
            os.path.abspath(__file__)), 'tests_data')
        city_csv = os.path.join(tests_data_dir, 'City.csv')
        with open(city_csv, newline='') as csvfile:
            citys = csv.reader(csvfile, delimiter=',')
            for city in citys:
                City.objects.create(
                    name=city[0], image=city[1])

        review_csv = os.path.join(tests_data_dir, 'Review.csv')
        with open(review_csv, newline='') as csvfile:
            reviews = csv.reader(csvfile, delimiter=',')
            for review in reviews:
                try:
                    image = review[3]
                except:
                    image = ""
                Review.objects.create(
                    city=review[0], traveller_name=review[1], traveller_review=review[2], image=image)

    def test_hello_world(self):
        response = self.client.get(reverse('hello_world'))
        self.assertEqual(response.data, "hello world!")

    def test_get_reviews_normal(self):
        response = self.client.get(reverse('get_reviews', kwargs={'page': 1}))
        reviews = Review.objects.all()[0:4]
        serializer = ReviewSerializer(reviews, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(len(response.data), 4)
        self.assertEqual(len(serializer.data), 4)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_reviews_no_image(self):
        # 5th review does no image, page2 first item
        response = self.client.get(reverse('get_reviews', kwargs={'page': 2}))
        reviews = Review.objects.all()[4:8]
        serializer = ReviewSerializer(reviews, many=True)
        self.assertNotEqual(response.data, serializer.data)
        self.assertEqual(serializer.data[0]['image'], '')
        self.assertEqual(response.data[0]['image'], 'Montreal.jpg')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_reviews_no_data(self):
        response = self.client.get(reverse('get_reviews', kwargs={'page': 5}))
        reviews = Review.objects.all()[16:20]
        serializer = ReviewSerializer(reviews, many=True)
        self.assertEqual(len(response.data), 0)
        self.assertEqual(len(serializer.data), 0)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
