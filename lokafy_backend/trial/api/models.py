from django.db import models

# Create your models here.


class City(models.Model):
    image = models.TextField(blank=True)
    name = models.CharField(max_length=100)


class Review(models.Model):
    image = models.TextField(blank=True)
    detail = models.TextField(blank=True)
    city = models.CharField(max_length=100)
    traveller_name = models.CharField(max_length=100)
    traveller_review = models.TextField(blank=True)
