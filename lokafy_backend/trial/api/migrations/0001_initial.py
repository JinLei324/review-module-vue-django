# Generated by Django 3.1.4 on 2020-12-25 04:40

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.TextField(blank=True)),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.TextField(blank=True)),
                ('detail', models.TextField(blank=True)),
                ('city', models.CharField(max_length=100)),
                ('traveller_name', models.CharField(max_length=100)),
                ('traveller_review', models.TextField(blank=True)),
            ],
        ),
    ]
