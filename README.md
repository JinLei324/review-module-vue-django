# Lokafy trial project



## 1. Front-end

Lokafy front-end use Vue.js.

- Create Vue project

```bash
$ vue create lokafy-frontend
```

- Install node dependencies

```bash
$ yarn install
```

- Add bootstrap ui framework

```bash
$ yarn add vue bootstrap-vue bootstrap 
```

Then, register BootstrapVue in your app entry point:

```javascript
//app.js
import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
```

And import Bootstrap and BootstrapVue `css` files:

```javascript
//app.js
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
```

- File Structure

```
/lokafy_frontend/
├── package.json
├── public
│   ├── favicon.ico
│   └── index.html
├── src
│   ├── App.vue
│   ├── assets
│   │   └── logo.png
│   ├── components //define components
│   │   ├── ReviewCard.vue
│   │   └── SiteNav.vue
│   ├── main.js
│   ├── router //set routers
│   │   └── index.js
│   └── views  //define views
│       ├── Home.vue
│       └── Reviews.vue
└── yarn.lock

```

- Views and components

*App.vue*

```vue
<template>
  <div id="app">
    <SiteNav></SiteNav>
    <router-view />
  </div>
</template>

<script>
import SiteNav from "@/components/SiteNav";

export default {
  name: "App",
  components: {
    SiteNav,
  },
};
```

use vue-router to show views

*Add vue-router*

```bash
$ yarn add vue-router
```

*SiteNav component*

Display website navigation menu

*Home View*

Display home page. Just show "home worked" string

*Review View*

Display review cards

```vue
//Review.vue
...
<b-col
        cols="12"
        md="6"
        class="mb-4"
        v-for="review in reviews"
        :key="review.id"
>
  <ReviewCard
              :image="review.image"
              :city="review.city"
              :id="review.id"
              :traveller_name="review.traveller_name"
              :traveller_review="review.traveller_review"
              />
</b-col>
<script>
import ReviewCard from "@/components/ReviewCard";
export default {
  name: "App",
  components: {
    ReviewCard,
  },
  data() {
    return {
      reviews: [], //Save review cards info
      ...
    };
  },
...
```



*ReviewCard component*

Show individual review card

```vue
<template>
  <b-row class="car-img-container ">
    <b-col cols="6" md="12" class="clearfix">
      <div class="limit">
        <a :href="detail">
          <img :src="image" alg="Image" />
        </a>
      </div>
    </b-col>
    <b-col cols="6" md="12">
      <b-card class="bg-light border-0 review-box">
        <div class="text-review-box">
          <a :href="detail">
            <p class="text-review text-left">
              {{ traveller_review }} 
            </p>
          </a>
          <a :href="detail" class="read-more text-right">
            <p>+read more</p>
          </a>
        </div>

        <div class="text-left mt-3">
          <h2 class="review-author">{{ traveller_name }}</h2>
          <p class="mt-1">Tour of {{ city }}</p>
        </div>
      </b-card>
    </b-col>
  </b-row>
</template>

<script>
export default {
  props: {
    image: String,
    id: Number,
    city: String,
    traveller_name: String,
    traveller_review: String,
  },
  computed: {
    // return detail url according to review id
    detail: function() {      
      return `reviews/detail/${this.id}`;
    },
  },
};
```



*Fetch Data from background*

use axios

```bash
$ yarn add axios
```

First, Review view mounted fetch data

```vue
<script>
...
import axios from "axios";

export default {
  name: "App",
  components: {
    ReviewCard,
  },
  data() {
    return {
      reviews: [],
      ...
      page: 1,
      ...
    };
  },
  ...
  mounted() {
    let reviewsAPI = `http://127.0.0.1:8000/api/reviews/${this.page}/`; //backend api call
    axios.get(reviewsAPI).then((response) => {
      this.reviews = response.data;
    });
  },
};
</script>
```

When click more reviews, increase page and fetch data to add reviews array

```vue
<template>
  ...
    
        <b-button class="more-reviews border-0" @click="loadMoreReview"
          >More Reviews</b-button
        >  //when click more reviews data, call loadMoreReview func
  ...   
  
</template>
<script>
import ReviewCard from "@/components/ReviewCard";
import axios from "axios";

export default {
  ...
  methods: {
    loadMoreReview() {
      this.page = this.page + 1;
      let reviewsAPI = `http://127.0.0.1:8000/api/reviews/${this.page}/`; //fectch data from page
      axios.get(reviewsAPI).then((response) => {
        if (response.data.length < 4) { //when no more data, hide more review button
          this.showMoreReviews = false;
        }
        response.data.forEach((item) => {
          this.reviews.push(item);
        });
      });
    },
  },
 ...
};
</script>

```



## 2. Lokafy Back-end

Use Django and Django Rest Framework

- Create Docker Image

```dockerfile
FROM python:3
# Set up and activate virtual environment
ENV VIRTUAL_ENV "/venv"
RUN python -m venv $VIRTUAL_ENV
ENV PATH "$VIRTUAL_ENV/bin:$PATH"
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED=1

#set workdir
WORKDIR /code

# install dependencies
RUN pip install --upgrade pip
COPY ./trial/requirements.txt /code
RUN pip install -r requirements.txt

# copy project
COPY . /code
```

- create docker-compose.yml

```yaml
version: "3.9"

services:
  db: //postgress image
    image: postgres
    environment:
      - POSTGRES_DB=postgres
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=postgres
  web:
    build: .
    command: bash -c "python trial/manage.py makemigrations && python trial/manage.py migrate && python trial/manage.py runserver 0.0.0.0:8000"
    volumes:
      - .:/code
    ports:
      - "8000:8000"
    depends_on:
      - db
```

- set python dependencies

```
//requirements.txt
Django>=3.0,<4.0     //Django
psycopg2-binary>=2.8 // for postgresql
djangorestframework  //django rest framework
django-cors-headers  //django cors policy
```

- Create Django project

```bash
$ docker-compose run web django-admin startproject trial ./trial
```

- Create API django app

```shell
$ docker-compose run web django-admin startapp api ./trial
```

- File Structure

```
/lokafy_backend/
├── Dockerfile //define docker image
├── confs 
│   └── config_file.json //define config info, like database access info
├── docker-compose.yml 
├── initial_data.json  //init data for review
└── trial
    ├── api              //app for backend api
    │   ├── admin.py
    │   ├── apps.py    
    │   ├── models.py
    │   ├── serializers.py
    │   ├── tests.py
    │   ├── tests_data
    │   │   ├── City.csv //include city test datas
    │   │   └── Review.csv //include review test datas
    │   ├── urls.py
    │   └── views.py
    ├── manage.py
    ├── requirements.txt
    └── trial        
        ├── asgi.py
        ├── settings.py
        ├── urls.py
        └── wsgi.py

```

- Database settings

```json
{
    "DATABASE_NAME": "postgres",
    "DATABASE_USER":"postgres",
    "DATABASE_PASS":"postgres",
    "DATABASE_HOST":"db",
    "DATABASE_PORT":5432
}
```

- set Postgres access

```python
# trial/setting.py
with open(os.path.join(os.path.dirname(BASE_DIR), 'confs/config_file.json')) as config_file:
    CONFIG_FILE = json.load(config_file)
    
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': CONFIG_FILE['DATABASE_NAME'],
        'USER': CONFIG_FILE['DATABASE_USER'],
        'PASSWORD': CONFIG_FILE['DATABASE_PASS'],
        'HOST': CONFIG_FILE['DATABASE_HOST'],
        'PORT': CONFIG_FILE['DATABASE_PORT'],
    }
}
```

- set api, rest_framework, cors for django

```python
# trial/setting.py
INSTALLED_APPS = [
    ...
    'corsheaders',
    'rest_framework',
    'api',
]
MIDDLEWARE = [
    ...
    'corsheaders.middleware.CorsMiddleware',
    ...

]
CORS_ALLOW_ALL_ORIGINS = True #allow all sites
```

- Define Models

```python
# trial/api/models.py
from django.db import models

# Create your models here.


class City(models.Model):
    image = models.TextField(blank=True) #city Image
    name = models.CharField(max_length=100) #city name


class Review(models.Model):
    image = models.TextField(blank=True)   #review image
    detail = models.TextField(blank=True)  #review detail, reserved
    city = models.CharField(max_length=100) #review city
    traveller_name = models.CharField(max_length=100) #review traveller name
    traveller_review = models.TextField(blank=True)  #review text
```

- Define Serializer for api json response

```python
#trial/api/serializers.py
from rest_framework import serializers
from .models import Review


class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = ('id', 'image', 'detail', 'city',
                  'traveller_name', 'traveller_review')
```

- Define REST endpoint

```python
#trial/api/views.py
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .models import City
from .models import Review
from .serializers import ReviewSerializer

# Create your views here.


@api_view(['GET'])
def hello_world(request): #Just for test
    return Response("hello world!")


@api_view(['GET'])
def get_reviews(request, page): #Return Review Data according to page, one page contain 4 reviews.
    reviews = Review.objects.all()[(page-1)*4:page*4]
    for review in reviews:
        if(review.image == ""): #if review has no image, use city image
            city = City.objects.get(name=review.city)
            review.image = city.image
    serializer = ReviewSerializer(reviews, many=True)
    return Response(serializer.data)
```



- set api url pattern

```python
#trial/api/urls.py
from django.urls import path
from .views import hello_world, get_reviews
urlpatterns = [
    path('hello/', hello_world, name='hello_world'),
    path('reviews/<int:page>/', get_reviews, name='get_reviews')
]
```

- set global url pattern

```python
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('api.urls')),
]
```

- Test DRF using pytest

```python
#trial/api/tests.py
import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from .models import Review
from .models import City
from .serializers import ReviewSerializer
import csv
import os


class TestRest(TestCase):
    def setUp(self):
        self.client = Client()

        # Read Test datas from csv
        tests_data_dir = os.path.join(os.path.dirname(
            os.path.abspath(__file__)), 'tests_data')
        city_csv = os.path.join(tests_data_dir, 'City.csv')
        with open(city_csv, newline='') as csvfile:
            citys = csv.reader(csvfile, delimiter=',')
            for city in citys:
                City.objects.create(
                    name=city[0], image=city[1])

        review_csv = os.path.join(tests_data_dir, 'Review.csv')
        with open(review_csv, newline='') as csvfile:
            reviews = csv.reader(csvfile, delimiter=',')
            for review in reviews:
                try:
                    image = review[3]
                except:
                    image = ""
                Review.objects.create(
                    city=review[0], traveller_name=review[1], traveller_review=review[2], image=image)

    def test_hello_world(self): #Just test hello world
        response = self.client.get(reverse('hello_world'))
        self.assertEqual(response.data, "hello world!")

    def test_get_reviews_normal(self): #Test page 1, all reviews has image
        response = self.client.get(reverse('get_reviews', kwargs={'page': 1}))
        reviews = Review.objects.all()[0:4]
        serializer = ReviewSerializer(reviews, many=True)

        self.assertEqual(response.data, serializer.data) #check response and serializer result
        self.assertEqual(len(response.data), 4) #check length
        self.assertEqual(len(serializer.data), 4)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_reviews_no_image(self):
        # 5th review does no image, page2 first item
        response = self.client.get(reverse('get_reviews', kwargs={'page': 2}))
        reviews = Review.objects.all()[4:8]
        serializer = ReviewSerializer(reviews, many=True)
        self.assertNotEqual(response.data, serializer.data)
        self.assertEqual(serializer.data[0]['image'], '') #check review image is empty
        self.assertEqual(response.data[0]['image'], 'Montreal.jpg') #check review image equal to city image
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_reviews_no_data(self): #page is large, so result is empty
        response = self.client.get(reverse('get_reviews', kwargs={'page': 5}))
        reviews = Review.objects.all()[16:20]
        serializer = ReviewSerializer(reviews, many=True)
        self.assertEqual(len(response.data), 0) #check length is 0
        self.assertEqual(len(serializer.data), 0) #check length is 0
        self.assertEqual(response.status_code, status.HTTP_200_OK)

```



## 3. Run lokafy_backend

-  Build docker image, import initial data

```bash
$ cd lokafy_backend 
$ docker-compose up -d --build
$ docker-compose exec web python trial/manage.py loaddata initial_data.json 
```

- run backend

```bash
$ docker-compose up
```

- run test

```bash
$ docker-compose exec web python trial/manage.py test api.tests
```



## 4. Run lokafy_frontend

- install node modules

```bash
$ cd lokafy_frontend
$ yarn intall
```

- run front-end

```bash
$ yarn serve
```

