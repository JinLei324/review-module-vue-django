from rest_framework.response import Response
from rest_framework.decorators import api_view
from .models import City
from .models import Review
from .serializers import ReviewSerializer

# Create your views here.


@api_view(['GET'])
def hello_world(request):
    return Response("hello world!")


@api_view(['GET'])
# Return Review Data according to page, one page contain 4 reviews.
def get_reviews(request, page):
    reviews = Review.objects.all()[(page-1)*4:page*4]
    for review in reviews:
        if(review.image == ""):  # if review has no image, use city image
            city = City.objects.get(name=review.city)
            review.image = city.image
    serializer = ReviewSerializer(reviews, many=True)
    return Response(serializer.data)
